--postgres 9.6
INSERT INTO shared_shelf.link_unlink (count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (1, 'SSN', '9000345339', 'SHARED', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (1, 'SSW', '8000648395', null, 183257, 10003);
INSERT INTO shared_shelf.link_unlink (count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (1, 'SSW', '8000648377', null, 183257, 10003);
INSERT INTO shared_shelf.link_unlink (count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (1, 'TGN', '4000001194', 'SHARED', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (1, 'AAT', '300404238', null, 183257, 10003);
INSERT INTO shared_shelf.link_unlink (count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (1, 'MFCL', '1603176', 'SHARED', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (1, 'MFCL', '2033837', 'LOCAL', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (0, 'MFCL', '1603176', 'LOCAL', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (1, 'MFCL', '9000343859', 'SHARED', 183257, 10003);


--postgres 8.0.2
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (1, 1, 'SSN', '9000345339', 'SHARED', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (2, 1, 'SSW', '8000648395', null, 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (3, 1, 'SSW', '8000648377', null, 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (4, 1, 'TGN', '4000001194', 'SHARED', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (5, 1, 'AAT', '300404238', null, 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (6, 1, 'MFCL', '1603176', 'LOCAL', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (7, 1, 'MFCL', '2033837', 'SHARED', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (8, 1, 'MFCL', '1603176', 'LOCAL', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (9, 1, 'SSW', '8000648377', null, 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (10, 0,'MFCL', '9000343859', 'SHARED', 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (11, 1, 'SSW', '8000052063', null, 183257, 10003);
INSERT INTO shared_shelf.link_unlink (id, count, vocab_type, vocab_level, vocab_id, user_id, institute_id) VALUES  (12, 1, 'SSW', '8000052064', null, 183257, 10003);

