-- Table: publish_targets

DROP TABLE IF EXISTS shared_shelf.publish_target;

CREATE TABLE shared_shelf.publish_target (
  id integer NOT NULL,
  PRIMARY KEY (id),
  name character varying(100)
);
