-- Table: institutions

DROP TABLE IF EXISTS shared_shelf.institution;

CREATE TABLE shared_shelf.institution (
  id integer NOT NULL,
  PRIMARY KEY (id),
  name character varying(100)
);
