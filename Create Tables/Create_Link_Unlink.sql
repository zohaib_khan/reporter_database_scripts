-- Table: link_unlink

--postgres 9.6
DROP TABLE IF EXISTS shared_shelf.link_unlink;
CREATE SEQUENCE IF NOT EXISTS shared_shelf.link_unlink_id_seq;
ALTER SEQUENCE shared_shelf.link_unlink_id_seq RESTART WITH 1;

CREATE TABLE shared_shelf.link_unlink (
  id integer NOT NULL DEFAULT nextval('shared_shelf.link_unlink_id_seq'),
  PRIMARY KEY (id),
  vocab_type character varying(15),
  vocab_level character varying(15),
  count integer,
  vocab_id character varying(15),
  user_id integer,
  institute_id integer
);


--postgres 8.0.2
CREATE TABLE shared_shelf.link_unlink (
  id integer NOT NULL,
  PRIMARY KEY (id),
  vocab_type character varying(15),
  vocab_level character varying(15),
  count integer,
  vocab_id character varying(15),
  user_id integer,
  institute_id integer,
);