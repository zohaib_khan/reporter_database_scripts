-- Table: projects

DROP TABLE IF EXISTS shared_shelf.project;

CREATE TABLE shared_shelf.project (
  id integer NOT NULL,
  PRIMARY KEY (id),
  name character varying(100)
);