  -- Table: no_media

--postgres 9.6
DROP TABLE IF EXISTS shared_shelf.no_media;
CREATE SEQUENCE IF NOT EXISTS shared_shelf.no_media_id_seq;
ALTER SEQUENCE shared_shelf.no_media_id_seq RESTART WITH 1;
CREATE TABLE shared_shelf.no_media (
  id integer NOT NULL DEFAULT nextval('shared_shelf.no_media_id_seq'),
  PRIMARY KEY (id),
  project_id integer,
  asset_id character varying(15),
  event_type_id integer,
  user_id integer,
  institute_id integer,
  created_on timestamp with time zone,
  created_by integer,
  last_updated_on timestamp with time zone,
  last_updated_by integer,
  FOREIGN KEY (event_type_id) REFERENCES shared_shelf.event_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (user_id) REFERENCES shared_shelf.user (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (institute_id) REFERENCES shared_shelf.institution (id) ON UPDATE NO ACTION ON DELETE NO ACTION
);



--postgres 8.0.2
CREATE TABLE shared_shelf.no_media (
  id integer NOT NULL,
  PRIMARY KEY (id),
  project_id integer,
  asset_id character varying(15),
  event_type_id integer,
  user_id integer,
  institute_id integer,
  created_on timestamp with time zone,
  created_by integer,
  last_updated_on timestamp with time zone,
  last_updated_by integer,
  FOREIGN KEY (event_type_id) REFERENCES shared_shelf.event_type (id),
  FOREIGN KEY (user_id) REFERENCES shared_shelf.user (id),
  FOREIGN KEY (institute_id) REFERENCES shared_shelf.institution (id)
);