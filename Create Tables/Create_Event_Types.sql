-- Table: event_types

--postgres 9.6
DROP TABLE IF EXISTS shared_shelf.event_type;
CREATE SEQUENCE IF NOT EXISTS shared_shelf.event_type_id_seq;
ALTER SEQUENCE shared_shelf.event_type_id_seq RESTART WITH 1;

CREATE TABLE shared_shelf.event_type (
  id integer NOT NULL DEFAULT nextval('event_type_id_seq'),
  PRIMARY KEY (id),
  event character varying(50)
);


--postgres 8.0.2
CREATE TABLE shared_shelf.event_type (
  id integer NOT NULL,
  PRIMARY KEY (id),
  event character varying(50)
);
