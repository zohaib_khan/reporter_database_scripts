  -- Table: no_media

--postgres 9.6
DROP TABLE IF EXISTS shared_shelf.event_history;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE shared_shelf.event_history (
  id uuid NOT NULL,
  PRIMARY KEY (id),
  event_type_id integer,
  vocab_id character varying(15),
  vocab_type character varying(15),
  user_id integer,
  institute_id integer,
  time_stamp timestamp with time zone,
  project_id integer,
  has_media boolean,
  publish_target_id integer,
  linked_to_wr boolean,
  FOREIGN KEY (event_type_id) REFERENCES shared_shelf.event_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (user_id) REFERENCES shared_shelf.user (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (institute_id) REFERENCES shared_shelf.institution (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (project_id) REFERENCES shared_shelf.project (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  FOREIGN KEY (publish_target_id) REFERENCES shared_shelf.publish_target (id) ON UPDATE NO ACTION ON DELETE NO ACTION
);


--postgres 8.0.2
CREATE TABLE shared_shelf.event_history (
  id character varying(256) NOT NULL,
  PRIMARY KEY (id),
  event_type_id integer,
  vocab_id character varying(15),
  vocab_type character varying(15),
  user_id integer,
  institute_id integer,
  time_stamp timestamp with time zone,
  project_id integer,
  has_media boolean,
  publish_target_id integer,
  linked_to_wr boolean,
  FOREIGN KEY (event_type_id) REFERENCES shared_shelf.event_type (id),
  FOREIGN KEY (user_id) REFERENCES shared_shelf.user (id),
  FOREIGN KEY (institute_id) REFERENCES shared_shelf.institution (id),
  FOREIGN KEY (project_id) REFERENCES shared_shelf.project (id),
  FOREIGN KEY (publish_target_id) REFERENCES shared_shelf.publish_target (id)
);
