-- Table: users

DROP TABLE IF EXISTS shared_shelf.user;

CREATE TABLE shared_shelf.user (
  id integer NOT NULL,
  PRIMARY KEY (id),
  email character varying(100)
);
